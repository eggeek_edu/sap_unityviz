﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Surface))]
public class VRAutowalk : MonoBehaviour {
	
	private CharacterController controller;
	private GvrViewer gvrViewer;
	private Transform vrHead;
	public int posIndex;
	public float speed = 0.3f;
	public float rSpeed = 1f;
	public Vector3[] aspects;
	public Vector3[] relativeLook;
	public Surface sur;
	public float rX, rY, rZ;
	public bool forward = false;
	// Use this for initialization
	void Start () {
		posIndex = 0;
		controller = GetComponent<CharacterController> ();
		gvrViewer = transform.GetChild (0).GetComponent<GvrViewer> ();
		vrHead = Camera.main.transform;
		aspects = new Vector3[5];
		relativeLook = new Vector3[5];
		rX = rY = rZ = 0f;
		UpdateSurface ();
	}

	public void UpdateSurface() {
		if (sur == null)
			return;
		float tan = Mathf.Tan (1f/6f * Mathf.PI);
		aspects [0] = new Vector3 (sur.xSize / 2f, 0, -tan*sur.xSize/2f);
		relativeLook [0] = new Vector3 (sur.xSize / 2f, 0, 0) - aspects [0];

		aspects [1] = new Vector3 (sur.xSize / 2f, 0, sur.zSize + tan*sur.xSize/2f);
		relativeLook [1] = new Vector3 (sur.xSize / 2f, 0, sur.zSize) - aspects [1];

		aspects [2] = new Vector3 (-tan * sur.zSize/2f, 0, sur.zSize / 2f);
		relativeLook [2] = new Vector3 (0, 0, sur.zSize / 2f) - aspects [2];

		aspects [3] = new Vector3 (sur.xSize + tan * sur.zSize/2f, 0, sur.zSize / 2f);
		relativeLook [3] = new Vector3 (sur.xSize, 0, sur.zSize / 2f) - aspects[3];

		aspects [4] = new Vector3 (sur.xSize / 2f, Mathf.Max (sur.zSize, sur.xSize) / 2f * tan, sur.zSize / 2f);
		relativeLook [4] = new Vector3 (sur.xSize / 2f, 0, sur.zSize / 2f) - aspects [4];
	}
	// Update is called once per frame
	void Update () {
//		UpdateSurface ();
//		if (Input.GetKey(KeyCode.F)) {
//			posIndex = (posIndex + 1) % 5;
//			controller.transform.position = aspects [posIndex];
//			Quaternion rotation = Quaternion.LookRotation (relativeLook[posIndex]);
//			Camera.main.transform.rotation = rotation;
//		}
		if (Input.GetMouseButtonDown(0)) {
			forward = !forward;
		}
		else if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
			forward = !forward;
		if (forward) controller.Move (vrHead.TransformDirection(Vector3.forward) * speed);
		if (Input.GetKey (KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
			controller.Move (vrHead.TransformDirection(Vector3.forward) * speed);
		}
		if (Input.GetKey (KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
			controller.Move (vrHead.TransformDirection(Vector3.back) * speed);
		}
		if (Input.GetKey (KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
			controller.Move (vrHead.TransformDirection(Vector3.left) * speed);
		}
		if (Input.GetKey (KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
			controller.Move (vrHead.TransformDirection(Vector3.right) * speed);
		}
		if (Input.GetKey(KeyCode.Q)) {
			rY += rSpeed;
			controller.transform.rotation = Quaternion.Euler (rX, rY, rZ);
		}
		if (Input.GetKey(KeyCode.E)) {
			rY -= rSpeed;
			controller.transform.rotation = Quaternion.Euler (rX, rY, rZ);
		}
		if (Input.GetKey(KeyCode.U)) {
			rX += rSpeed;
			controller.transform.rotation = Quaternion.Euler (rX, rY, rZ);
		}
		if (Input.GetKey(KeyCode.B)) {
			rX -= rSpeed;
			controller.transform.rotation = Quaternion.Euler (rX, rY, rZ);
		}
	}
}
