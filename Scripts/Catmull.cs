﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class Catmull {
	public float alpha = 0.5f;
	public float GetT(float t, Vector2 p0, Vector2 p1) {
		float a = Mathf.Pow((p1.x-p0.x), 2.0f) + Mathf.Pow((p1.y-p0.y), 2.0f);
		float b = Mathf.Pow(a, 0.5f);
		float c = Mathf.Pow(b, alpha);
		return (c + t);
	}
	public float[] GetTs(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3) {
		float[] t = new float[4];
		t[0] = 0f;
		t[1] = GetT(0, p0, p1);
		t[2] = GetT(t[1], p1, p2);
		t[3] = GetT(t[2], p2, p3);
		return t;
	}
	public float GetInterpolationPoint(float t, float[] ts, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3) {
		float t0 = ts [0], t1 = ts [1], t2 = ts [2], t3 = ts [3];
		Vector2 A1 = (t1-t)/(t1-t0)*p0 + (t-t0)/(t1-t0)*p1;
		Vector2 A2 = (t2-t)/(t2-t1)*p1 + (t-t1)/(t2-t1)*p2;
		Vector2 A3 = (t3-t)/(t3-t2)*p2 + (t-t2)/(t3-t2)*p3;

		Vector2 B1 = (t2-t)/(t2-t0)*A1 + (t-t0)/(t2-t0)*A2;
		Vector2 B2 = (t3-t)/(t3-t1)*A2 + (t-t1)/(t3-t1)*A3;

		Vector2 C = (t2-t)/(t2-t1)*B1 + (t-t1)/(t2-t1)*B2;
		return C.y;
	}
}
