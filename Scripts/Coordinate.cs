﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Coordinate: MonoBehaviour {

  public int debugX = 1;
  public int debugZ = 1;
  public int xSize = 10;
  public int zSize = 10;
  public int edgeSize = 0;
  public int xScale = 1;
  public int zScale = 1;
  public float yScale = 1f;
  private Mesh mesh;
  //public Vector3[] vertices;
  public List<Vector3> vertices;
  public List<int> coordinate_lines;
  public List<int> boundary_lines;
  public List<int> scale_lines;

  public Dictionary<Pii, float> data;
  public int[,] grid;
  public int maxRow;
  public int maxCol;
  public float maxY;
  public float minY;
  public Pii maxYarg;
  public int degreeY = 4;
  public int segments = 10;
  private void Awake () {
    LoadData ();
    Generate();
  }
  public TextAsset source;

  private void LoadData() {
    data = new Dictionary<Pii, float> ();
    maxRow = maxCol = 0;
    maxYarg = new Pii(-1, -1);
    string textFile = source.text;
    string[] file_lines = textFile.Split ('\n');// System.IO.File.ReadAllLines ("Assets/out.txt");
      foreach (string line in file_lines) {
      int x = int.Parse (line.Split (',')[0]);
      int z = int.Parse (line.Split (',')[1]);
      float c = float.Parse (line.Split (',') [2]) / yScale;
      maxRow = Math.Max (maxRow, x);
      maxCol = Math.Max (maxCol, z);
      if (!data.ContainsKey(new Pii (x, z)))
        data [new Pii (x, z)] = 0;
      data [new Pii (x, z)] += c;
    }
    maxY = -1e10f;
    minY = 1e10f;
    for (int r = 1; r <= maxRow; r++) {
      for (int c = 1; c <= maxCol; c++) {
        Pii k = new Pii (r, c);
        if (!data.ContainsKey (k))
          data [k] = 0f;
        if (data[k] > maxY) {
          maxY = data[k];
          maxYarg = new Pii(r, c);
        }
        minY = Math.Min (minY, data [k]);
      }
    }

    print ("minY: " + minY.ToString ());
    print ("maxY: " + maxY.ToString ());
    print(String.Format("maxYarg: {0}, {1}", maxYarg.x, maxYarg.y));
    Shader.SetGlobalFloat ("_MaxHeight", maxY);
    Shader.SetGlobalFloat ("_MinHeight", maxY);

    xSize = edgeSize * 2 + maxRow * xScale;
    zSize = edgeSize * 2 + maxCol * zScale;
  }

  private void Generate() {
    GetComponent<MeshFilter>().mesh = mesh = new Mesh();
    mesh.name = "Procedural Coordinate";
    mesh.subMeshCount = 3;
    GenerateVertexes();
    GenerateBoundaryLinesByLineRenderer();
    //GenerateBoundaryLines();
    GenerateCoordinateLinesByLineRenderer();
    //GenerateCoordinateLines();
    GenerateScaleLinesByLineRenderer();
    //GenerateScaleLines();
    GenerateScaleLabel();
  }

  private void GenerateVertexes() {
    vertices = new List<Vector3>();

    // idx in [0, xSize * zSize - 1] are coordinate vertexes
    for (int x = 0; x < xSize; x++) {
      for (int z = 0; z < zSize; z++) {
        vertices.Add(new Vector3(x, 0, z));
      }
    }
    // idx = xSize * zSize, y axis header vertex
    vertices.Add(new Vector3(0, maxY, 0));
    // scale vertex on x-axis: [xSize * zSize + 1, xSize * zSize + maxRow]
    for (int r = 0; r < maxRow; r++) vertices.Add(new Vector3(r * xScale, 0, -1));
    // scale vertex on z-axis: [xSize * zSize + maxRow + 1, xSize * zSize + maxRow + maxCol]
    for (int c = 0; c < maxCol; c++) vertices.Add(new Vector3(xSize, 0, c * zScale));
    // scale vertex on y-axis: [xSize * zSize + maxRow + maxCol + 1, xSize * zSize + maxRow + maxCol + 2 * degreeY]
    float differ = maxY - minY;
    for (int i=1; i<=degreeY; i++) {
      float y = minY + differ / (float)degreeY * (float)i;
      vertices.Add(new Vector3(0, y, 0));
      vertices.Add(new Vector3(0, y, zSize-1));
    }
    // minY: [xSize * zSize + maxRow + maxCol + 2 * degreeY + 1]
    vertices.Add(new Vector3(0, minY, 0));
    // location of maxY: [xSize * zSize + maxRow + maxCol + 2 * degreeY + 2]
    int arg_x = (maxYarg.x - 1) * xScale;
    int arg_z = (maxYarg.y - 1) * zScale;
    vertices.Add(new Vector3(arg_x, maxY, arg_z));
    mesh.vertices = vertices.ToArray();
    print ("vert size: " + vertices.Count.ToString ());
  }

  private void GenerateCoordinateLinesByLineRenderer() {
    Material mat = GetComponent<MeshRenderer>().materials[0];
    for (int z = 1; z < zSize; z++) {
      int vid0 = z;
      int vid1 = (xSize - 1) * zSize + z;
      CreateLineByLineRender(vid0, vid1, "coord-z:" + z.ToString(), mat);
    }
    for (int x = 0; x + 1 < xSize; x++) {
      int vid0 = x * zSize;
      int vid1 = x * zSize + zSize - 1;
      CreateLineByLineRender(vid0, vid1, "coord-x:" + x.ToString(), mat);
    }
  }

  private void GenerateCoordinateLines() {
    coordinate_lines = new List<int>();
    int idx = 0;
    for (int z = 1; z < zSize; z++) {
      int vid0 = z;
      int vid1 = (xSize - 1) * zSize + z;
      coordinate_lines.Add(vid0);
      coordinate_lines.Add(vid1);
    }
    for (int x = 0; x + 1 < xSize; x++) {
        int vid0 = x * zSize;
        int vid1 = x * zSize + zSize - 1;
        coordinate_lines.Add(vid0);
        coordinate_lines.Add(vid1);
    }
    mesh.SetIndices (coordinate_lines.ToArray(), MeshTopology.Lines, 0);
  }

  private GameObject CreateLineByLineRender(int vid0, int vid1, String title, Material mat) {
    GameObject line = new GameObject(title);
    LineRenderer l = line.AddComponent<LineRenderer>();
    l.material = mat;
    l.SetWidth(0.1f, 0.1f);
    l.SetVertexCount(segments + 1);
    int idx = 0;
    Vector3 v0 = vertices[vid0];
    Vector3 v1 = vertices[vid1];
    l.SetPosition(idx++, v0);
    for (int i = 1; i < segments; i++) {
      Vector3 v = v0 + (v1 - v0) * (float)i / (float)segments;
      l.SetPosition(idx++, v);
    }
    l.SetPosition(idx++, v1);
    return line;
  }

  private void GenerateBoundaryLinesByLineRenderer() {
    Material mat = GetComponent<MeshRenderer>().materials[1];
    CreateLineByLineRender(0, (xSize - 1) * zSize, "x-axis", mat);
    CreateLineByLineRender(
      (xSize - 1) * zSize,
      (xSize - 1) * zSize + zSize - 1, "z-axis", mat);
    CreateLineByLineRender(
      xSize * zSize + maxRow + maxCol + 2 * degreeY + 1,
      xSize * zSize, "y-axis", mat);
    int vid = xSize * zSize + maxRow + maxCol;
    for (int i=1; i<=degreeY; i++) {
      CreateLineByLineRender(
        vid + 2 * i - 1,
        vid + 2 * i, "height-" + i.ToString(), mat);
    }
  }

  private void GenerateBoundaryLines() {
    boundary_lines = new List<int>();
    int idx = 0;
    boundary_lines.Add(0);
    boundary_lines.Add((xSize - 1) * zSize);

    boundary_lines.Add((xSize -1) * zSize);
    boundary_lines.Add((xSize - 1) * zSize + zSize - 1);

    boundary_lines.Add(xSize * zSize + maxRow + maxCol + 2 *degreeY + 1);
    boundary_lines.Add(xSize * zSize);

    int vid = xSize * zSize + maxRow + maxCol;
    for (int i=1; i<=degreeY; i++) {
      boundary_lines.Add(vid + 2 * i - 1);
      boundary_lines.Add(vid + 2 * i);
    }

    mesh.SetIndices(boundary_lines.ToArray(), MeshTopology.Lines, 1);
  }

  private void GenerateScaleLinesByLineRenderer() {
    Material mat = GetComponent<MeshRenderer>().materials[2];
    for (int r = 0; r < maxRow; r++) {
      int vid0 = r * xScale * zSize;
      int vid1 = xSize * zSize + 1 + r;
      CreateLineByLineRender(vid0, vid1, "row"+r.ToString(), mat);
    }
    for (int c = 0; c < maxCol; c++) {
      int vid0 = (xSize - 1) * zSize + c * zScale;
      int vid1 = (xSize * zSize) + maxRow + 1 + c;
      CreateLineByLineRender(vid0, vid1, "col"+c.ToString(), mat);
    }
    CreateLineByLineRender(
      xSize * zSize + maxRow + maxCol + 2 * degreeY + 2,
      xSize * zSize, "max-height", mat);
  }

  private void GenerateScaleLines() {
    scale_lines = new List<int>();
    int idx = 0;
    for (int r = 0; r < maxRow; r++) {
      int vid0 = r * xScale * zSize;
      int vid1 = xSize * zSize + 1 + r;
      scale_lines.Add(vid0);
      scale_lines.Add(vid1);
    }
    for (int c = 0; c < maxCol; c++) {
      int vid0 = (xSize - 1) * zSize + c * zScale;
      int vid1 = xSize * zSize + maxRow + 1 + c;
      scale_lines.Add(vid0);
      scale_lines.Add(vid1);
    }
    int x = (maxYarg.x - 1) * xScale;
    int z = (maxYarg.y - 1) * zScale;
    scale_lines.Add(xSize * zSize + maxRow + maxCol + 2 * degreeY + 2);
    scale_lines.Add(xSize * zSize);
    mesh.SetIndices(scale_lines.ToArray(), MeshTopology.Lines, 2);
  }

  private void GenerateScaleLabel() {
    Vector3 xLabelRotation = new Vector3 (0, 0, 0);
    for (int r = 0; r < maxRow; r++) {
      createLabel((r+1).ToString(), new Vector3(r * xScale, 0, -2), xLabelRotation);
    }
    Vector3 zLabelRotation = new Vector3 (0, -90, 0);
    for (int c = 0; c < maxCol; c++) {
      createLabel((c+1).ToString(), new Vector3(xSize + 1, 0, c * zScale), zLabelRotation);
    }
    for (int i=1; i<=degreeY; i++) {
      float y = minY + (maxY - minY) / (float)degreeY * (float)i;
      //createLabel(string.Format("{0}/{1} of maxY", i, degreeY), new Vector3(0, y, -2), zLabelRotation);
      createLabel(string.Format("{0}", y * yScale), new Vector3(0, y, -2), zLabelRotation);
    }

  }

  GameObject createLabel(string label, Vector3 position, Vector3 rotation) {
    GameObject TextObject = new GameObject(label);
    TextObject.AddComponent<TextMesh>();
    TextMesh tm = TextObject.GetComponent<TextMesh>();
    tm.text = label;
    TextObject.transform.localPosition = position;
  TextObject.transform.eulerAngles = rotation;
    TextObject.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    tm.fontSize = 108;
    tm.color = Color.black;
    TextObject.AddComponent<BoxCollider>();

    TextObject.transform.parent = transform;

    return TextObject;
  }

  private void SetVertex(int i, int x, float y, int z) {
    vertices [i] = new Vector3 (x, y, z);
  }

  public int DataRow(int gridX) {
    return (gridX - edgeSize) / xScale + 1;
  }

  public int DataCol(int gridZ) {
    return (gridZ - edgeSize) / zScale + 1;
  }

  public int GridX(int dataRow, int ord) { // ord in [0, xScale)
    return edgeSize + (dataRow -1)* xScale + ord;
  } 

  public int GridZ(int dataCol, int ord) { // ord in [0, zScale)
    return edgeSize + (dataCol-1) * zScale + ord;
  }

  Vector2 Get2DPointXY(int gx, int gz) {
    if (gx >= edgeSize && gx < xSize - edgeSize) {
      int vid = grid [gx, gz];
      return new Vector2 (gx, vertices [vid].y);
    } else
      return new Vector2 (gx, 0);
  }

  Vector2 Get2DPointZY(int gx, int gz){
    if (gz >= edgeSize && gz < zSize - edgeSize) {
      int vid = grid [gx, gz];
      return new Vector2 (gz, vertices [vid].y);      
    } else
      return new Vector2 (gz, 0);
  }
  void Update() {
    Vector3 v = Camera.main.transform.position;
    for (int i = 0; i < transform.childCount; i++) {
      GameObject it = this.gameObject.transform.GetChild (i).gameObject;
      Vector3 relative = it.transform.position - v;
      Quaternion rotation = Quaternion.LookRotation (relative);
      it.transform.rotation = rotation;

    }
  }
}
