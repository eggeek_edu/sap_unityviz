﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Surface : MonoBehaviour {

  public int debugX = 1;
  public int debugZ = 1;
  public int xSize = 10;
  public int zSize = 10;
  public int edgeSize = 0;
  public int xScale = 1;
  public int zScale = 1;
  public float yScale = 1f;
  private Mesh mesh;
  public Vector3[] vertices;
  private Vector3[] normals;
  private Color32[] cubeUV;
  public int[] triangles;

  public Dictionary<Pii, float> data;
  public int[,] grid;
  public int maxRow;
  public int maxCol;
  public float maxY;
  public float minY;
  public Catmull spline;
  public TextAsset source;
  public int segments = 50;

  private void Awake () {
    LoadData ();
    Generate();
    Recalculate ();
    SetUpGrid();
  }

  private void LoadData() {
    data = new Dictionary<Pii, float> ();
    maxRow = maxCol = 0;
  string[] lines = source.text.Split ('\n'); //System.IO.File.ReadAllLines ("Assets/out.txt");
    foreach (string line in lines) {
      int x = int.Parse (line.Split (',')[0]);
      int z = int.Parse (line.Split (',')[1]);
      float c = float.Parse (line.Split (',') [2]) / yScale;
      maxRow = Math.Max (maxRow, x);
      maxCol = Math.Max (maxCol, z);
      if (!data.ContainsKey(new Pii (x, z)))
        data [new Pii (x, z)] = 0;
      data [new Pii (x, z)] += c;
    }
    maxY = -1e10f;
    minY = 1e10f;
    for (int r = 1; r <= maxRow; r++) {
      for (int c = 1; c <= maxCol; c++) {
        Pii k = new Pii (r, c);
        if (!data.ContainsKey (k))
          data [k] = 0f;
        maxY = Math.Max (maxY, data [k]);
        minY = Math.Min (minY, data [k]);
      }
    }

    print ("minY: " + minY.ToString ());
    print ("maxY: " + maxY.ToString ());
    Shader.SetGlobalFloat ("_MaxHeight", maxY);
    Shader.SetGlobalFloat ("_MinHeight", maxY);

    xSize = edgeSize * 2 + maxRow * xScale;
    zSize = edgeSize * 2 + maxCol * zScale;
  }

  private void BindDataWithGrid() {
    grid = new int[xSize + 1, zSize + 1];
    for (int i = 0; i < xSize; i++)
      for (int j = 0; j < zSize; j++)
        grid [i, j] = -1;
    for (int vertexIndex=0; vertexIndex < xSize * zSize; vertexIndex++) {
      Pii v = new Pii ((int)vertices [vertexIndex].x, (int)vertices [vertexIndex].z);
      grid [v.x, v.y] = vertexIndex;
    }
  }

  private void Generate () {
    GetComponent<MeshFilter>().mesh = mesh = new Mesh();
    mesh.name = "Procedural Grid";

    vertices = new Vector3[xSize * zSize];
    normals = new Vector3[vertices.Length];
    cubeUV = new Color32[vertices.Length];

    print ("vert size: " + vertices.Length.ToString ());
    for (int x = 0; x < xSize; x++) {
      for (int z = 0; z < zSize; z++) {
        SetVertex (x * zSize + z, x, 0, z);
      }
    }
    mesh.vertices = vertices;
    triangles = new int[(xSize-1) * (zSize-1) * 12];
    for (int ti = 0, vi = 0, x = 0; x+1 < xSize; x++, vi++) {
      for (int z = 0; z + 1 < zSize; z++, vi++) {
        ti = SetSquare (ti, vi, vi + 1, vi + zSize + 1, vi + zSize);
        ti = SetSquare (ti, vi + zSize, vi + zSize + 1, vi + 1, vi);
      }
    }
    mesh.subMeshCount = 2;
    mesh.SetTriangles (triangles, 0);
    mesh.SetTriangles (triangles, 1);
    mesh.SetIndices (triangles, MeshTopology.Triangles, 0);
    int[] lines = new int[2 * (xSize - 1) * zSize + 2 * (zSize -1) * xSize];
    int idx = 0;
    for (int z = 0; z < zSize; z++) {
      for (int x = 0; x + 1 < xSize; x++) {
        int vid = x * zSize + z;
        lines [idx++] = vid;
        lines [idx++] = vid + zSize;
      }
    }
    for (int x = 0; x < xSize; x++) {
      for (int z = 0; z + 1 < zSize; z++) {
        int vid = x * zSize + z;
        lines [idx++] = vid;
        lines [idx++] = vid + 1;
      }
    }
    //mesh.SetIndices (lines, MeshTopology.Lines, 1);
    BindDataWithGrid ();
  }

  private GameObject RenderXaxis(int x) {
    GameObject line = new GameObject(String.Format("x:{0}", x));
    LineRenderer gline = line.AddComponent<LineRenderer>();
    Material mat = GetComponent<MeshRenderer>().materials[1];
    gline.material = mat;
    gline.SetWidth(0.05f, 0.05f);
    gline.SetVertexCount((zSize - 1) * (segments + 1));
    int idx = 0;
    for (int z = 0; z + 1 < zSize; z++) {
      int vid = x * zSize + z;
      Vector3 v0 = vertices[vid];
      Vector3 v1 = vertices[vid + 1];
      gline.SetPosition(idx++, v0);
      for (int i = 1; i < segments; i++) {
        Vector3 v = v0 + (v1 - v0) * (float)i / (float)segments;
        gline.SetPosition(idx++, v);
      }
      gline.SetPosition(idx++, v1);
    }
    return line;
  }

  private GameObject RenderZaxis(int z) {
    GameObject line = new GameObject(String.Format("z:{0}", z));
    LineRenderer gline = line.AddComponent<LineRenderer>();
    Material mat = GetComponent<MeshRenderer> ().materials [1];
    gline.material = mat;
    gline.SetWidth (0.05f, 0.05f);
    gline.SetVertexCount((xSize - 1) * (segments + 1));
    int idx = 0;
    for (int x = 0; x + 1 < xSize; x++) {
      int vid = x * zSize + z;
      Vector3 v0 = vertices [vid];
      Vector3 v1 = vertices[vid + zSize];
      gline.SetPosition (idx++, v0);
      for (int i = 1; i < segments; i++) {
        Vector3 v = v0 + (v1 - v0) * (float)(i) / (float)segments;
        gline.SetPosition (idx++, v);
      }
      gline.SetPosition (idx++, v1);
    }
    return line;
  }

  private void SetUpGrid() {
    for (int z = 0; z < zSize; z++) RenderZaxis(z);
    for (int x = 0; x < xSize; x++) RenderXaxis(x);
  }

  private void SetVertex(int i, int x, int y, int z) {
    vertices [i] = new Vector3 (x, y, z);
    cubeUV [i] = new Color32 ((byte)x, (byte)y, (byte)z, 0);
  }

  private int SetSquare(int ti, int v0, int v1, int v2, int v3) {
    triangles [ti] = v0;
    triangles [ti + 1] = v1;
    triangles [ti + 2] = v2;
    triangles [ti + 3] = v0;
    triangles [ti + 4] = v2;
    triangles [ti + 5] = v3;
    return ti + 6;
  }


  public int DataRow(int gridX) {
    return (gridX - edgeSize) / xScale + 1;
  }

  public int DataCol(int gridZ) {
    return (gridZ - edgeSize) / zScale + 1;
  }

  public int GridX(int dataRow, int ord) { // ord in [0, xScale)
    return edgeSize + (dataRow -1)* xScale + ord;
  } 

  public int GridZ(int dataCol, int ord) { // ord in [0, zScale)
    return edgeSize + (dataCol-1) * zScale + ord;
  }


  private void UpdateVertex(int i, int x, float y, int z) {
    Vector3 inner = vertices[i] = new Vector3(x, y, z);
    normals[i] = (vertices[i] - inner).normalized;
    cubeUV[i] = new Color32((byte)x, (byte)y, (byte)z, 0);
  }


  Vector2 Get2DPointXY(int gx, int gz) {
    if (gx >= edgeSize && gx < xSize - edgeSize) {
      int vid = grid [gx, gz];
      return new Vector2 (gx, vertices [vid].y);
    } else
      return new Vector2 (gx, 0);
  }

  Vector2 Get2DPointZY(int gx, int gz){
    if (gz >= edgeSize && gz < zSize - edgeSize) {
      int vid = grid [gx, gz];
      return new Vector2 (gz, vertices [vid].y);      
    } else
      return new Vector2 (gz, 0);
  }

  private void Recalculate() {
    for (int row = 1; row <= maxRow; row++) {
      for (int col = 1; col <= maxCol; col++) {
        int gx = GridX (row, 0);
        int gz = GridZ (col, 0);
        UpdateVertex (grid[gx, gz], gx, data[new Pii(row, col)], gz);
        //print (String.Format ("gx:{0}, gz{1}, vid:{2} y:{3}", gx, gz, grid [gx, gz], vertices[grid[gx, gz]].y));

      }
    }
    spline = new Catmull ();
    Vector2 p0, p1, p2, p3;
    float[] ts = new float[4];
    // Z-Y slice
    for (int row = 1; row <= maxRow; row++) {
      int gx = GridX (row, 0);
      for (int col = 1; col <= maxCol; col++) {
        int gz = GridZ (col, 0);
        p0 = Get2DPointZY (gx, gz - zScale); p1 = Get2DPointZY (gx, gz);
        p2 = Get2DPointZY (gx, gz + zScale); p3 = Get2DPointZY (gx, gz + 2*zScale);
        ts = spline.GetTs (p0, p1, p2, p3);
        float diffT = (ts [2] - ts [1]) / (float)zScale;
        float t = ts[1];
        for (int j = 0; j < zScale; j++, t += diffT) {
          float c = spline.GetInterpolationPoint (t, ts, p0, p1, p2, p3);
          //print (String.Format("update, gx: {0}, gz: {1}, vid: {2}, origin y: {3}, new y: {4}", gx, gz, grid[gx, gz], vertices[grid[gx, gz]].y, c));
          UpdateVertex (grid [gx, gz], gx, c, gz);
          gz++;
        }
      }
    }

    // X-Y slice
    for (int col = 1; col <= maxCol; col++) {
      int gz = GridZ (col, 0);
      for (int row = 1; row <= maxRow; row++) {
        int gx = GridX (row, 0);
        p0 = Get2DPointXY (gx - xScale, gz); p1 = Get2DPointXY (gx, gz);
        p2 = Get2DPointXY (gx + xScale, gz); p3 = Get2DPointXY (gx + 2*xScale, gz);
        ts = spline.GetTs (p0, p1, p2, p3);
        float diffT = (ts [2] - ts [1]) / (float)xScale;
        float t = ts[1];
        for (int i = 0; i < xScale; i++, t += diffT) {
          float c = spline.GetInterpolationPoint (t, ts, p0, p1, p2, p3);
          //print (String.Format("update, gx: {0}, gz{1}, vid: {2}, origin y: {3}, new y: {4}", gx, gz, grid[gx, gz], vertices[grid[gx, gz]].y, c));
          UpdateVertex (grid [gx, gz], gx, c, gz);
          gx++;
        }
      }
    }

    for (int row = 1; row <= maxRow; row++) {
      for (int i = 1; i < xScale; i++) {
        int gx = GridX (row, i);
        for (int col = 1; col <= maxCol; col++) {
          int gz = GridZ (col, 0);
          p0 = Get2DPointZY (gx, gz - zScale); p1 = Get2DPointZY (gx, gz);
          p2 = Get2DPointZY (gx, gz + zScale); p3 = Get2DPointZY (gx, gz + 2*zScale);
          ts = spline.GetTs (p0, p1, p2, p3);
          float diffT = (ts [2] - ts [1]) / (float)zScale;
          float t = ts[1];
          for (int j = 0; j < zScale; j++, t += diffT) {
            float c = spline.GetInterpolationPoint (t, ts, p0, p1, p2, p3);
            UpdateVertex (grid [gx, gz], gx, c, gz);
            gz++;
          }
        }
      }
    }

    for (int col = 1; col <= maxCol; col++) {
      for (int j = 1; j < zScale; j++) {
        int gz = GridZ (col, j);
        for (int row = 1; row <= maxRow; row++) {
          int gx = GridX (row, 0);
          p0 = Get2DPointXY (gx - xScale, gz); p1 = Get2DPointXY (gx, gz);
          p2 = Get2DPointXY (gx + xScale, gz); p3 = Get2DPointXY (gx + 2*xScale, gz);
          ts = spline.GetTs (p0, p1, p2, p3);
          float diffT = (ts [2] - ts [1]) / (float)zScale;
          float t = ts[1];
          for (int i = 0; i < xScale; i++, t += diffT) {
            float c = spline.GetInterpolationPoint (t, ts, p0, p1, p2, p3);
//            print(String.Format("row{0}, col{1}, gx:{2}, gz:{3}, y:{4}", row, col, gx, gz, c));
            UpdateVertex (grid [gx, gz], gx, c, gz);
            gx++;
          }
        }
      }
    }
    mesh.vertices = vertices;
    mesh.RecalculateBounds ();
    mesh.RecalculateNormals ();
  }
}
