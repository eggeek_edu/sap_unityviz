﻿using UnityEngine;
using System;
using System.Collections.Generic;

public struct Pii {
	public int x, y;
	public Pii(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public override string ToString ()
	{
		return string.Format ("[Pii]{0}, {1}", x, y);
	}

	public override int GetHashCode() {
		return this.ToString ().GetHashCode ();
	}
}

public class Polynomial {

	private Dictionary<Pii, float> data;
	private SortedDictionary<int, float> samples;
	List<float> C;
	public Vector3[] points = new Vector3[1];
	public Vector3[] polyPoints = new Vector3[1];
	public float yScale = 2000f;
	public int xScale = 1;
	public int debugX = 11;
	public int diffY = 1;

	public void LoadData() {
		data = new Dictionary<Pii, float> ();
		string[] lines = System.IO.File.ReadAllLines ("out.txt");
		foreach (string line in lines) {
			int x = int.Parse (line.Split (',')[0]);
			int z = int.Parse (line.Split (',')[1]);
			float c = float.Parse (line.Split (',') [2]) / yScale;
			Pii key = new Pii (x, z);
			if (!data.ContainsKey(key))
				data [key] = 0;
			data [key] += c;
		}
	}

	private int ScaledX(int originX) {
		return (originX - 1) * xScale + 1;
	}

	private void Recalculate() {
		samples = new SortedDictionary<int, float> ();
		foreach (Pii v in data.Keys) {
			if (v.x != debugX)
				continue;
			samples.Add (v.y, data [v]);
		}
		int idxp = 0;
		points = new Vector3[samples.Count];
		polyPoints = new Vector3[samples.Count * xScale];
		foreach (int x in samples.Keys) {
			points [idxp++] = new Vector3 (ScaledX(x), samples [x], 0);
		}
		CatmulRom ();
	}

	Vector3 GetPoint(int idx) {
		if (idx < 0)
			return new Vector2 (idx, points [0].y);
		else if (idx >= points.Length)
			return new Vector2 (points [points.Length - 1].x + (idx - points.Length + 1), points [points.Length - 1].y);
		else
			return new Vector2 (points [idx].x, points [idx].y);
	}

	void CatmulRom()
	{
		List<Vector2> newPoints = new List<Vector2> ();
		float amountOfPoints = (float)xScale;
		float alpha = 0.5f;
		newPoints.Clear();

		Vector2 p0, p1, p2, p3;
		float t0, t1, t2, t3;

		for (int i = 0; i < points.Length; i++) {

			p0 = GetPoint (i - 1);
			p1 = GetPoint (i);
			p2 = GetPoint (i + 1);
			p3 = GetPoint (i + 2);

			t0 = 0.0f;
			t1 = GetT(t0, alpha, p0, p1);
			t2 = GetT(t1, alpha, p1, p2);
			t3 = GetT(t2, alpha, p2, p3);

			for (float t = t1; t < t2; t += (t2 - t1) / amountOfPoints) {
				Vector2 C = GetInterpolationPoint (p0, p1, p2, p3, t, alpha);
				newPoints.Add (C);
			}
		}

		polyPoints = new Vector3[newPoints.Count];
		int idx = 0;
		foreach (Vector2 v in newPoints) {
			polyPoints [idx++] = new Vector3 (v.x, v.y + diffY, 0f);
		}
	}

	Vector2 GetInterpolationPoint(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float t, float alpha) {

		float t0 = 0.0f;
		float t1 = GetT(t0, alpha, p0, p1);
		float t2 = GetT(t1, alpha, p1, p2);
		float t3 = GetT(t2, alpha, p2, p3);
		Vector2 A1 = (t1-t)/(t1-t0)*p0 + (t-t0)/(t1-t0)*p1;
		Vector2 A2 = (t2-t)/(t2-t1)*p1 + (t-t1)/(t2-t1)*p2;
		Vector2 A3 = (t3-t)/(t3-t2)*p2 + (t-t2)/(t3-t2)*p3;

		Vector2 B1 = (t2-t)/(t2-t0)*A1 + (t-t0)/(t2-t0)*A2;
		Vector2 B2 = (t3-t)/(t3-t1)*A2 + (t-t1)/(t3-t1)*A3;

		Vector2 C = (t2-t)/(t2-t1)*B1 + (t-t1)/(t2-t1)*B2;
		return C;
	}

	float GetT(float t, float alpha, Vector2 p0, Vector2 p1)
	{
		float a = Mathf.Pow((p1.x-p0.x), 2.0f) + Mathf.Pow((p1.y-p0.y), 2.0f);
		float b = Mathf.Pow(a, 0.5f);
		float c = Mathf.Pow(b, alpha);

		return (c + t);
	}

	//Visualize the points
	void OnDrawGizmos()
	{
		if (polyPoints == null || points == null)
			return;
		Gizmos.color = Color.blue;
		foreach(Vector2 temp in polyPoints) {
			Vector3 pos = new Vector3(temp.x, temp.y, 0);
			Gizmos.DrawSphere(pos, 0.3f);
		}
		Gizmos.color = Color.red;
		foreach (Vector2 temp in points) {
			Vector3 pos = new Vector3 (temp.x, temp.y, 0);
			Gizmos.DrawSphere (pos, 0.3f);
		}
	}

	public void Reset () {
		points = new Vector3[] {
			new Vector3(1f, 0f, 0f),
			new Vector3(2f, 0f, 0f),
			new Vector3(3f, 0f, 0f),
			new Vector3(4f, 0f, 0f)
		};
	}
}