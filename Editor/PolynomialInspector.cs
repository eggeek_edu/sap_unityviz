﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Surface))]
public class SurfaceInspector : Editor {

	private const int lineSteps = 10;
	private const float directionScale = 0.5f;

	private Surface sur;
	private Transform handleTransform;
	private Quaternion handleRotation;

	private void OnSceneGUI () {
		sur = target as Surface;
		if (sur == null)
			return;
		handleTransform = sur.transform;
		handleRotation = Tools.pivotRotation == PivotRotation.Local ?
			handleTransform.rotation : Quaternion.identity;
		Vector3 p0, p1;
		for (int row = 1; row <= sur.maxRow; row++) {
			if (row != sur.debugX)
				continue;
			int gx = sur.GridX (row, 0);
			for (int col = 1; col <= sur.maxCol; col++) {
				for (int j = 0; j < sur.zScale; j++) {
					int gz = sur.GridZ (col, j);
					p0 = sur.vertices [sur.grid[gx, gz]];
					if (gz + 1 < sur.zSize) {
						p1 = sur.vertices[sur.grid [gx, gz + 1]];
						Handles.color = Color.red;
						Handles.DrawLine (p0, p1);
					}
				}
			}
		}

		for (int col = 1; col <= sur.maxCol; col++) {
			if (col != sur.debugZ)
				continue;
			int gz = sur.GridX (col, 0);
			for (int row = 1; row <= sur.maxRow; row++) {
				for (int i = 0; i < sur.xScale; i++) {
					int gx = sur.GridX (row, i);
					p0 = sur.vertices [sur.grid [gx, gz]];
					if (gx + 1 < sur.xSize) {
						p1 = sur.vertices [sur.grid [gx + 1, gz]];
						Handles.color = Color.cyan;
						Handles.DrawLine (p0, p1);
					}
				}
			}
		}
	}

	private Vector3 ShowPoint (Vector3 _point) {
		Vector3 point = handleTransform.TransformPoint(_point);
		EditorGUI.BeginChangeCheck();
		point = Handles.DoPositionHandle(point, handleRotation);
		if (EditorGUI.EndChangeCheck()) {
			Undo.RecordObject(sur, "Move Point");
			EditorUtility.SetDirty(sur);
//			_point = handleTransform.InverseTransformPoint(point);
		}
		return point;
	}
	public override void OnInspectorGUI () {
		DrawDefaultInspector();
		sur = target as Surface;
	}
}