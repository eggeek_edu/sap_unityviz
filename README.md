# README #

[Demo](https://www.dropbox.com/s/nbydmugtqy91o2w/SAP_VR.unitypackage?dl=0)

### Components ###

* Model: *Scripts/{Coordinate.cs, Surface.cs}*

    Load data, draw surface and coordinate

* Spline: *Scripts/{Polynomial.cs, Catmull.cs}*
    
    Make the VR model looks smooth
    
* Charactor controller: *Scripts/VRAutowalk.cs*

    Be responsible for player movement and interaction
    
* Shader: *Shader/surface.shader*

### Issues ###

* iOS
    ![IMG_0777 2.PNG](https://bitbucket.org/repo/9x7bR6/images/3439420850-IMG_0777%202.PNG)
![IMG_0778.PNG](https://bitbucket.org/repo/9x7bR6/images/4148004793-IMG_0778.PNG)
    - Crash: [disable Metal API Validation](https://github.com/googlevr/gvr-unity-sdk/issues/331)
    - Doesn't show scene

        Sometimes program running without crash, but scene does show up.
        
* Android

    - Unexpected mesh in left camera: (iOS doesn't has this problem)
![1.png](https://bitbucket.org/repo/9x7bR6/images/880248588-1.png)
![2.png](https://bitbucket.org/repo/9x7bR6/images/2153394178-2.png)
  This is not happen in iOS and PC editor, so possible reasons may be: build setting, platform, phone itself.
  
    >May be due to using GL
line primitives which are not supported by the limited OpenGL
implementations on phones.

    - Camera angle drift

### TODO ###
* More interaction between player and model (selecting, changing viewing angle...)
* Highlight row/column slice in game view. (for example: red and green lines in picture below)
![Screen Shot 2017-02-14 at 16.20.45.png](https://bitbucket.org/repo/9x7bR6/images/2436346294-Screen%20Shot%202017-02-14%20at%2016.20.45.png)

### How to use ###


* If you just want to run demo:
  1. Create a new unity project;
  2. Download the unity package in attachment and import it;

* If you want to develop:
  1. Create a new unity project;
  2. Go to the project folder, clone the repo and rename to `Assets`:
    `git clone https://eggeek_edu@bitbucket.org/eggeek_edu/sap_unityviz.git Assets`
  3. Dowload the unity package in attachment and import it

