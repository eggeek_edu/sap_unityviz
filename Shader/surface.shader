﻿Shader "Surface" {
	Properties {
		_Color ("Color", Color) = (0,0,0,0)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_MaxHeight("Max Height", Float) = 0
		_MinHeight("Min Height", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		//#pragma shader_feature _FACES_Y _FACES_Z
		#pragma surface surf Standard fullforwardshadows vertex:vert
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 cubeUV;
			float4 pos;
			float height;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float _MaxHeight;
		float _MinHeight;
		
		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.pos = v.vertex;
			o.height = v.vertex.y;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.cubeUV) * _Color;
			// linear
//			float factor = (IN.height - _MinHeight + 1.0) / (_MaxHeight - _MinHeight + 1.0);
			// sigmod
			float x = IN.height - (_MaxHeight + _MinHeight) / 2.0;
			float factor = 1.0 / (1.0 + exp(-x));
			o.Albedo = c.rgb * factor;
			o.Albedo.r = c.r * factor;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = factor * c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
